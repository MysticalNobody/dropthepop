import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Кнопка на главном экране
///
/// Принимает:
/// [text] - текст кнопки
/// [color] - цвет заливки кнопки
/// [opacity] - прозрачность кнопки
class MainButton extends StatelessWidget {
  const MainButton(
      {required this.text,
      required this.color,
      required this.opacity,
      required this.onTap,
      super.key});
  final String text;
  final Color color;
  final double opacity;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: opacity,
      child: CupertinoButton(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        minSize: 0,
        onPressed: onTap,
        child: Container(
          height: 74,
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(20),
          ),
          alignment: Alignment.center,
          child: Text(
            text,
            style: const TextStyle(
                fontWeight: FontWeight.bold, fontSize: 17, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
