import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'widgets/main_button.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: const EdgeInsets.only(
                top: 23,
                left: 24,
                right: 24,
                bottom: 15,
              ),
              padding: const EdgeInsets.only(
                top: 14.9,
                left: 22.16,
                bottom: 16.25,
                right: 33.16,
              ),
              decoration: BoxDecoration(
                color: const Color(0xff1D1D1D),
                borderRadius: BorderRadius.circular(24),
              ),
              height: 109,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SvgPicture.asset('assets/icons/top_card_quote.svg'),
                    ],
                  ),
                  const Text(
                    'Anyday is a great\nday for a nice poop 💩',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 21,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SvgPicture.asset('assets/icons/bottom_card_quote.svg'),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 180,
              child: Image.asset('assets/images/home_pop.png'),
            ),
            Column(
              children: [
                MainButton(
                  text: 'No poop today(',
                  color: const Color(0xff1D1D1D),
                  opacity: .4,
                  onTap: () {
                    print('Hello World 1');
                  },
                ),
                const SizedBox(height: 12),
                MainButton(
                  text: 'Drop the poop',
                  color: const Color(0xffFF342E),
                  opacity: 1,
                  onTap: () {
                    print('Hello World 2');
                  },
                ),
                const SizedBox(height: 32),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 34, vertical: 16),
                  height: 95,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 46,
                        width: 46,
                        decoration: BoxDecoration(
                          color: const Color(0xffFF342E),
                          borderRadius: BorderRadius.circular(16),
                        ),
                        alignment: Alignment.center,
                        child:
                            SvgPicture.asset('assets/icons/bottom_bar_pop.svg'),
                      ),
                      Container(
                        height: 46,
                        width: 46,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                        ),
                        alignment: Alignment.center,
                        child: SvgPicture.asset(
                          'assets/icons/bottom_bar_pin.svg',
                          color: const Color(0xff2D2D2E),
                        ),
                      ),
                      Container(
                        height: 46,
                        width: 46,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                        ),
                        alignment: Alignment.center,
                        child: SvgPicture.asset(
                          'assets/icons/bottom_bar_list.svg',
                          color: const Color(0xff2D2D2E),
                        ),
                      ),
                      Container(
                        height: 46,
                        width: 46,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                        ),
                        alignment: Alignment.center,
                        child: SvgPicture.asset(
                          'assets/icons/bottom_bar_profile.svg',
                          color: const Color(0xff2D2D2E),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
