import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({
    required this.child,
    super.key,
  });
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: child,
    );
  }
}
