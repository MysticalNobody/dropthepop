import 'package:dropthepop/app.dart';
import 'package:flutter/material.dart';

import 'features/home/home_screen.dart';

void main() {
  runApp(
    const App(
      child: HomeScreen(),
    ),
  );
}
